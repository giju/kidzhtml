
function createConnection() {
    const IP = 'd.ceegees.in';
    const PORT = '3478'; //33533

    const iceServers = [{
        urls: `turn:${IP}:${PORT}?transport=tcp`,
        username: 'cgs',
        credential: 'cgs'
    }];

    const peer = new RTCPeerConnection({
        iceServers
    });


    const remoteStream = new MediaStream();
    const remoteVideo = document.getElementById('remote-video');
    if (remoteVideo) {
        remoteVideo.srcObject = remoteStream;
    }

    peer.addEventListener('track', event => {
        remoteStream.addTrack(event.track, remoteStream);
    });
    peer.onicecandidate = e => { 
        if (!e.candidate) {
            return;
        }
        $("#candidate-log").prepend('<div class="w3-border w3-padding"> ' +
            JSON.stringify(e.candidate)
            + '</div>')
        console.log('candidate update', e);
    };

    peer.oniceconnectionstatechange = e => {
        $("#output-log").prepend('<div class="w3-margin-bottom">Connection state change : '+e.target.connectionState+'</div>');
        console.log('Connection state: ',e);
        if (e.target.connectionState === 'failed' || e.target.iceConnectionState === 'disconnected') {
            $("#output-log").prepend('<div class="w3-margin-bottom">Restarting ice</div>'); 
            peer.restartIce();
        }
    };
    return peer;
}


async function createAnswer() {
    const offerStr = $("#offer-text-area").val();
    // $("#offer-text-area").prop('disabled',true);
    $("#offer-area").hide();
    const peer = createConnection();
    window.peer = peer;
    const offer = JSON.parse(offerStr);
    await peer.setRemoteDescription(offer);

    // $("#candidate-log").html('');
    navigator.getUserMedia({
        video: true,
        audio: true
    }, async (stream) => {

        stream.getVideoTracks().forEach((track) => {
            peer.addTrack(track, stream);
        });
        stream.getAudioTracks().forEach(async (track) => {
            peer.addTrack(track, stream);
        });

        const answer = await peer.createAnswer();
        await peer.setLocalDescription(answer);
        $("#answer-out-text-area").text(JSON.stringify(answer));
    })

}


function updateAnswer() {
    // $("#candidate-log").html('');
    const answer = $("#answer-text-area").val();
    // $("#answer-text-area").prop('disabled',true);
    window.peer.setRemoteDescription(JSON.parse(answer));
}

async function createOffer() {
    //get media , get stream , create peer connection , create offer , print add json to div.
    const peer = createConnection();
    var configuration = {
        iceRestart: true,
        offerToReceiveAudio: true,// type === 'audio',
        offerToReceiveVideo: true, //Ptype === 'video' || type === 'screen'
    }

    const offer = await peer.createOffer(configuration);
    await peer.setLocalDescription(offer);
    $("#offer-text").text(JSON.stringify(offer))
    $("#answer-area").hide();
    window.peer = peer;
}

function addIceCandidate() {
    const candidate = $("#add-ice-candidate").val();
    window.peer.addIceCandidate(JSON.parse(candidate));
    $("#add-ice-candidate").val('');
}